import {
  sendTutorRequestReciept,
  sendTutorRegistrationReceipt,
  sendErrorNotice
} from "./functions/mail";
import { putTutee, putTutor } from "./functions/database";
import {
  Course,
  strToDistinction,
  Tutee,
  Tutor,
  CourseObject
} from "./classes/index";

interface GoogleFormRequest {
  name: string;
  email: string;
}

interface TuteeFormRequest extends GoogleFormRequest {
  additionalEmails?: string;
  courseTopic: string;
  level: string;
  progression: string;
}

interface TutorFormRequest extends GoogleFormRequest {
  courseDeriverText: string;
}

export default function switchboard(request: any): string {
  if (request.courseTopic) {
    return processTuteeRequest(request as TuteeFormRequest);
  } else if (request.courseDeriverText) {
    return processTutorRequest(request as TutorFormRequest);
  }
  console.error("Could not interpret request");
  sendErrorNotice(new Error(`Could not interpret request ${request}`));
  return "Could not understand request.";
}

function processTuteeRequest(tuteeRequest: TuteeFormRequest): string {
  try {
    const course = new Course(
      tuteeRequest.courseTopic,
      strToDistinction(tuteeRequest.level),
      parseInt(tuteeRequest.progression, 10)
    );

    let additionalEmails: string[];
    if (tuteeRequest.additionalEmails) {
      additionalEmails = tuteeRequest.additionalEmails.split(",");
    } else {
      additionalEmails = [];
    }
    const tutee = new Tutee(
      course,
      tuteeRequest.name,
      tuteeRequest.email,
      additionalEmails
    );
    console.log(tutee.asObject);

    putTutee(tutee);
    sendTutorRequestReciept(tutee);

    return `Parsed tuteeRequest for ${tutee.name}`;
  } catch (e) {
    console.error("Whoops! Something went wrong!");
    console.error(e);
    sendErrorNotice(e);
    return "Failed to parse tuteeRequest.";
  }
}

function processTutorRequest(tutorRequest: TutorFormRequest): string {
  try {
    const courses: Course[] = [];
    (JSON.parse(tutorRequest.courseDeriverText) as CourseObject[]).forEach(
      element => {
        console.log(element);
        courses.push(Course.fromObject(element));
      }
    );

    console.log(courses);

    const tutor = new Tutor(courses, tutorRequest.name, tutorRequest.email);
    putTutor(tutor);
    sendTutorRegistrationReceipt(tutor);

    return `Parsed tutorRequest for ${tutor.name}`;
  } catch (e) {
    console.error("Whoops! Something went wrong!");
    console.error(e);
    sendErrorNotice(e);
    return "Failed to parse tutorRequest.";
  }
}
