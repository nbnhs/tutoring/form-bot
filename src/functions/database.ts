import { Firestore } from "@google-cloud/firestore";
import { Tutee, TuteeObject, Tutor, TutorObject } from "../classes/index";
import { sendTutorConfirmation, sendErrorNotice } from "./mail";

const db = new Firestore({
  projectId: process.env.FIRESTORE_PROJECT_ID,
  keyFilename: process.env.FIRESTORE_KEY_FILE_PATH
});

// db.collection.onSnapshot runs every time the collection in question is updated.

db.collection("tutors").onSnapshot(
  _ => {
    console.log("Update to the tutors collection detected!");
    matchAll();
  },
  err => {
    console.log(`Encountered error: ${err.message}`);
    sendErrorNotice(err);
  }
);

let tuteesCallbackCount = 0;
db.collection("tutees").onSnapshot(
  _ => {
    if (tuteesCallbackCount === 0) {
      // No need to run matchAll twice initially (both onSnapshot functions run on startup)
      tuteesCallbackCount++;
      return;
    }

    console.log("Update to the tutees collection detected!");
    matchAll();
  },
  err => {
    console.log(`Encountered error: ${err.message}`);
    sendErrorNotice(err);
  }
);

/**
 * Put a Tutee into the database.
 * @param tutee The tutee to put into the database. Assumes a unique name to each Tutee. No duplicates exist; tutee will be updated if it already exists.
 */
export function putTutee(tutee: Tutee) {
  db.collection("tutees")
    .doc(`${tutee.name}`)
    .set(tutee.asObject);
}

/**
 * Put a Tutor into the database.
 * @param tutor The tutee to put into the database. Assumes a unique name to each Tutor. No duplicates exist; tutor will be updated if it already exists.
 */
export function putTutor(tutor: Tutor) {
  db.collection("tutors")
    .doc(`${tutor.name}`)
    .set(tutor.asObject);
}

/**
 * Finds a tutee matches among the current pool of tutors.
 * @param tutee The tutee to find a match for
 * @returns A promise for matches
 */
export async function findMatches(tutee: Tutee): Promise<Tutor[]> {
  let tutors = [];
  for (const tutor of await getAllTutors()) {
    console.log(
      `Attempting to pair ${tutor.name} and ${tutee.name}`
    );
    if (tutee.match(tutor)) {
      tutors.push(tutor);
    } else {
      console.log(`Not matched.`);
    }
  }
  return tutors;
}

/**
 * Attempt to match all unmatched tutees with tutors.
 */
export async function matchAll() {
  const query = await db
    .collection("tutees")
    .where("tutorFound", "==", false)
    .get();

  query.forEach(doc => {
    const tutee = Tutee.fromObject(doc.data() as TuteeObject);

    if (tutee.tutorFound) {
      return;
    }

    console.log(
      `${tutee.name} hasn't been matched yet; trying to find all matches...`
    );

    findMatches(tutee).then(tutors => {
      if (tutors.length > 0) {
        const asString = tutors.join("; ");
        console.log(
          `Matched ${tutee.name} with the following: ${asString}`
        );

        putTutee(tutee);
        sendTutorConfirmation(tutee, tutors);
      }
    });
  });

  console.log("matchAll complete.");
}

/**
 * Download all tutors from the database and convert them to Tutor objects.
 * @returns An array of all tutors currently in the database
 */
export async function getAllTutors(): Promise<Tutor[]> {
  const result = await db.collection("tutors").get();
  const arr: Tutor[] = [];
  result.forEach(doc => {
    arr.push(Tutor.fromObject(doc.data() as TutorObject));
  });
  console.log(`Processed ${arr.length} tutors.`);
  return arr;
}
