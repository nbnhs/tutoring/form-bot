import mailgun from "mailgun-js";
import { Tutee, Tutor } from "../classes/index";

const mail = mailgun({
  apiKey: String(process.env.MAILGUN_API_KEY),
  domain: String(process.env.MAILGUN_DOMAIN)
});

/**
 * Send a reciept verifying that the tutee has requested a tutor.
 * @param tutee The tutee to send the reciept to.
 */
export function sendTutorRequestReciept(tutee: Tutee) {
  const message = `Hello ${
    tutee.name
  },\n\nThank you for requesting a tutor from the National Honor Society peer tutoring service.\n\nWe will email you when we find you a tutor for your course.\n\nSincerely yours,\n\nMilo Gilad\nNHS President & Programmer`;

  const onError = function(
    err: mailgun.Error,
    body: mailgun.messages.SendResponse
  ) {
    if (err) {
      console.log(
        `Error while trying to send a tutor request reciept: ${err.message}`
      );
      return;
    }
    console.log(body);
  };

  if (tutee.otherEmails.length > 0) {
    mail.messages().send(
      {
        from: `mail@${process.env.MAILGUN_DOMAIN}`,
        to: tutee.email,
        cc: tutee.otherEmails.join(","),
        subject: "Your NHS Peer Tutoring Request",
        text: message
      },
      onError
    );
  } else {
    mail.messages().send(
      {
        from: `mail@${process.env.MAILGUN_DOMAIN}`,
        to: tutee.email,
        subject: "Your NHS Peer Tutoring Request",
        text: message
      },
      onError
    );
  }
}

/**
 * Send a reciept verifying that the tutor has registered with the system.
 * @param tutor The tutor to send the reciept to.
 */
export function sendTutorRegistrationReceipt(tutor: Tutor) {
  mail.messages().send(
    {
      from: `mail@${process.env.MAILGUN_DOMAIN}`,
      to: tutor.email,
      subject: "Your NHS Tutor Registration",
      text: `Hello ${tutor.name},\n\nThank you for registering to tutor students with the National Honor Society. You will receive an email when you have been paired with a student seeking help.\n\nSincerely yours,\n\nMilo Gilad\nNHS President & Programmer`
    },
    (err, body) => {
      if (err) {
        console.log(
          `Error while trying to send a tutor registration reciept: ${err.message}`
        );
        return;
      }
      console.log(body);
    }
  );
}

/**
 * Send an email to the sysadmin notifying them of a new pairing.
 * @param tutee The tutee who was just paired.
 * @param tutor The tutor who was just paired.
 */
export function sendTutorConfirmation(tutee: Tutee, tutors: Tutor[]) {
  const tutorsMsg = tutors.map((tutor) => `${tutor.name} (${tutor.email})\n`);
  mail.messages().send(
    {
      from: `mail@${process.env.MAILGUN_DOMAIN}`,
      to: String(process.env.SYSADMIN_EMAIL),
      subject: "New Pairing",
      text: `The following persons have been paired:\n\nTutee: ${tutee.name} (${tutee.email})\n\nTutors:\n${tutorsMsg}`
    },
    (err, body) => {
      if (err) {
        console.log(
          `Error while trying to send a pairing confirmation: ${err.message}`
        );
        return;
      }
      console.log(body);
    }
  );
}

/**
 * Notify the sysadmin of a system error
 * @param err Error to notify about.
 */
export function sendErrorNotice(err: Error) {
  mail.messages().send(
    {
      from: `mail@${process.env.MAILGUN_DOMAIN}`,
      to: String(process.env.SYSADMIN_EMAIL),
      subject: "System Error",
      text: `MESSAGE\n\n${err.message}\n\n\n\nSTACK\n\n${err.stack}`
    },
    (err, body) => {
      if (err) {
        console.log(
          `Error while trying to send an error notification (ironic!): ${err.message}`
        );
        return;
      }
      console.log(body);
    }
  );
}
