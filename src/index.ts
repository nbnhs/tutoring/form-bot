import express from "express";
import switchboard from "./switchboard";

const app = express();
app.use(
  express.urlencoded({
    extended: true
  })
);

app
  .post("/", (request, response) => {
    console.log("Got a request!");
    console.log(request.body);

    response.end(switchboard(request.body));
  })
  .listen(process.env.PORT);
