import { Distinction } from "./Distinction";

export interface CourseObject {
  topic: string;
  difficulty: number;
  progression: number;
}

/**
 * Contains the information for a single course offered at the school.
 *
 * Includes difficulty/distinction, topic/subject, and the progession number (e.g. the 3 in Spanish 3).
 */
export class Course {
  private _topic: string;
  private _difficulty: Distinction;
  private _progression: number;

  /**
   * Converts a string representation of a course into a Course object.
   * @param s String in the format "Topic Progression Distinction" (e.g. "Spanish 3 Honors")
   */
  static fromString(s: string): Course {
    const arr: string[] = s.split(" ");
    const dist: Distinction = (<any>Distinction)[arr[2]];
    return new this(arr[0], dist, parseInt(arr[1]));
  }

  constructor(topic: string, difficulty: Distinction, progression?: number) {
    this._topic = topic;
    this._difficulty = difficulty;
    if (!progression) {
      this._progression = 1;
    } else {
      this._progression = progression;
    }
  }

  get topic(): string {
    return this._topic;
  }

  get difficulty(): Distinction {
    return this._difficulty;
  }

  get progression(): number {
    return this._progression;
  }

  /**
   * String representation of the course.
   * @returns String in the format "Topic Progression Distinction" (e.g. Spanish 3 Honors)
   */
  public toString(): string {
    if (this.progression == 0) {
      return `${this.topic} ${Distinction[this.difficulty]}`;
    }
    return `${this.topic} ${this.progression} ${Distinction[this.difficulty]}`;
  }

  /**
   * Determines if this course is a subset of another.
   *
   * A course is a subset if the other course has a curriculum that encompases the entirety of this course's.
   *
   * For example: Spanish 3 Regular is a subset of Spanish 3 Honors because Honors covers all the material in Regular (and then some), SL Physics is a subset of HL Physics, and so on.
   * @param c The course to compare the current course against
   * @returns true if this course is a subset of `c`, false otherwise
   */
  public subsetOf(c: Course): boolean {
    return (
      c.topic == this.topic &&
      c.progression >= this.progression &&
      c.difficulty >= this.difficulty
    );
  }

  /**
   * Converts the Course object to a more easily serialized object.
   * @returns A new CourseObject from the current Course
   */
  get asObject(): CourseObject {
    return {
      topic: this._topic,
      difficulty: this._difficulty,
      progression: this._progression
    };
  }

  /**
   * Converts the simplified CourseObject back to an instance of the Course class.
   * @param obj A simplified CourseObject instance.
   */
  static fromObject(obj: CourseObject): Course {
    return new Course(obj.topic, obj.difficulty, obj.progression);
  }
}
