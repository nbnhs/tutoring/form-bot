/**
 * Enumerates the various difficulty levels of courses offered, in least-difficult to most-difficult order.
 *
 * **NOTE**: SL and HL cannot directly compare to AP, although both are more difficult than Honors and Regular.
 */
export enum Distinction {
  Regular,
  Honors,
  AP,
  SL,
  HL
}

export function strToDistinction(str: string): Distinction {
  switch (str) {
    case "Honors":
      return Distinction.Honors;
    case "AP":
    case "Advanced Placement":
      return Distinction.AP;
    case "SL":
    case "IB Standard-Level":
      return Distinction.SL;
    case "HL":
    case "IB Higher-Level":
      return Distinction.HL;
    case "Regular":
    default:
      return Distinction.Regular;
  }
}
