import { Course, CourseObject } from "./Course";

export interface TutorObject {
  name: string;
  email: string;
  courses: CourseObject[];
}

/**
 * Describes a potential tutor for any tutoree.
 *
 * A tutor has a history of courses (`_courses`) taken that can qualify them to tutor another student in certain material.
 */
export class Tutor {
  constructor(
    public courses: Course[],
    public name: string,
    public email: string
  ) {}

  /**
   * Converts the Tutor object to a more easily serialized object.
   * @returns A new TutorObject from the current Tutor
   */
  get asObject(): TutorObject {
    return {
      name: this.name,
      email: this.email,
      courses: this.courses.map(course => {
        return course.asObject;
      })
    };
  }

  /**
   * Converts the simplified TutorObject back to an instance of the Tutor class.
   * @param obj A simplified TutorObject instance.
   */
  static fromObject(obj: TutorObject): Tutor {
    return new Tutor(
      obj.courses.map(courseObj => {
        return Course.fromObject(courseObj);
      }),
      obj.name,
      obj.email
    );
  }
}
