import { Course, CourseObject } from "./Course";
import { Tutor } from "./Tutor";

export interface TuteeObject {
  student: {
    name: string;
    email: string;
  };
  otherEmails: string[];
  course: CourseObject;
  tutorFound: boolean;
}

/**
 * Describes a tutoree requesting help in a Course `_request`.
 */
export class Tutee {
  public tutorFound: boolean;

  constructor(
    public request: Course,
    public name: string,
    public email: string,
    public otherEmails: string[]
  ) {
    this.tutorFound = false;
  }

  /**
   * Determines if a tutoree can be reasonably matched up with a provided tutor.
   *
   * A tutor is qualified to tutor a tutoree if that tutor has experience in the requested course. This can be determined by seeing if any of the courses in the tutor's history are supersets of the course which help is needed in.
   * @param t Tutor to check for a match with
   * @returns true if the tutor and tutoree can be paired, false otherwise
   */
  public match(t: Tutor): boolean {
    const match =
      t.courses.findIndex(
        (value: Course): boolean => {
          return this.request.subsetOf(value);
        }
      ) != -1;
    this.tutorFound = match;
    return match;
  }

  /**
   * Converts the Tutee object to a more easily serialized object.
   * @returns A new TuteeObject from the current Tutee
   */
  get asObject(): TuteeObject {
    return {
      student: {
        name: this.name,
        email: this.email
      },
      otherEmails: this.otherEmails,
      course: this.request.asObject,
      tutorFound: this.tutorFound
    };
  }

  /**
   * Converts the simplified TuteeObject back to an instance of the Tutee class.
   * @param obj A simplified TuteeObject instance.
   */
  static fromObject(obj: TuteeObject): Tutee {
    return new Tutee(
      Course.fromObject(obj.course),
      obj.student.name,
      obj.student.email,
      obj.otherEmails
    );
  }
}
