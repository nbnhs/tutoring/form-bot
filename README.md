# NHS Peer Tutoring Program Form Response Bot

Responds to POST requests containing a request for a tutor. Matches a tutee with a pool of tutors, and coordinates tutoring sessions between them.

## Installation/Setup

This API is intended to run on a Heroku instance. Learn more about Heroku [here](https://www.heroku.com).

Set the following environment variables for the API to operate correctly:

- `MAILGUN_API_KEY`: your API key from [Mailgun](https://mailgun.com) (used to send mail)
- `MAILGUN_DOMAIN`: a verified domain on Mailgun
- `FIRESTORE_PROJECT_ID`: the Google Cloud project ID to use with Google Firestore
- `FIRESTORE_KEY_FILE`: JSON text containing your Firestore access key/service account. Use [this Quickstart page](https://cloud.google.com/firestore/docs/quickstart-servers#set-up-authentication) to create a service account and corresponding file.
- `FIRESTORE_KEY_FILE_PATH`: the path to output the `FIRESTORE_KEY_FILE` to while the Heroku instance is working (necessary because the key cannot be passed as an environment variable).
- `SYSADMIN_EMAIL`: the system administrator's email (for notifications).

## Usage

A request will come from a Google Form (which forwards the response via Google Apps Script).

This request does the following once placed:

- Sends a reciept to the tutee's email plus any additionally specified emails confirming that their request was placed
- Puts the tutee into the pool of tutees

Then, since the database has listeners for changes to the tutee pool, it will attempt to match the tutee with a tutor in the current pool. If a match can't be made, it will try again when the pool of tutors changes. If a match is made, the sysadmin is notified.
